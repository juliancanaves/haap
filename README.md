HAAP 
========

Hacker Appetite
------------------

Copyright 
Julian Canaves <julian.canaves@gmail.com>
Doukeshi Factory <doukeshi@doukeshi.com>

License in progress


ABOUT
-------------------------------
A conversational graphic adventure in which you are a cyberspace hacker.
You interact with the game by entering commands in the console (no mouse). 

Main objective of the game is to execute `hack shutdown` at the System CPU Node.
To learn the mechanics you should start with Tutorials: `easy`, `medium` and `hard`.
Also by reading the Wiki: https://bitbucket.org/juliancanaves/haap/wiki/Home
Request an account by giving feedback in this form: http://goo.gl/forms/8gCRu0Yu3L


Install
----------------------
# Make a Workspace directory, clont the 3 repositories 'haap-react', 'haap-maps', 'haap-assets'.

# Run `npm install` in 'haap-react' directory.

# Add the following lines to your .bashrc adjusting as you need
alias cd-haap="cd /path/to/haapworkspace/haap-react"
alias start-haap-assets="cd-haap; cd ../haap-assets; bash start_server.sh"
alias start-haap-maps="cd-haap; cd ../haap-maps; supervisor index.js"
alias start-haap="cd-haap; npm start"

# run `start-haap-assets` , `start-haap-maps`, `start-haap`  in diferent terminals

