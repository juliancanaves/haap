import React from 'react';
import ReactDOM from 'react-dom';
import Haap from './Haap';
//import registerServiceWorker from './registerServiceWorker';
import './index.css';
require('dotenv').config();

ReactDOM.render(<Haap />, document.getElementById('root'));
// registerServiceWorker();