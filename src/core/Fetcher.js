
const DEBUG = false;

export const fetchMapServer = async (action, raw = false) => {
    var mapServer = process.env.REACT_APP_MAP_SERVER;

    if (DEBUG) console.log('URL : '+mapServer+'/'+action);
    if (DEBUG) console.log('requested raw: '+raw);

    try {
        var response = await fetch(mapServer+'/'+action);

        if(undefined !== response && response.ok) {
            return raw ? response.text() : response.json();
        }

    } catch (err) {
        console.log('ERROR: '+err);
        return undefined;
    }

    return null;    
}

export async function fetchAsset(path) {
    var assetsServer = process.env.REACT_APP_ASSETS_SERVER;

    if (DEBUG) console.log('URL : '+assetsServer+'/'+path);

    try {
        var response = await fetch(assetsServer+'/'+path);

        if(undefined !== response && response.ok) {
            return response.text();
        }

    } catch (err) {
        console.log('ERROR: '+err);
        return '<p>Asset '+path+' not found '+err+'</p>';
    }
}