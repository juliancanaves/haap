import { DECKERS } from "../core/Consts";

export function makeCharacter(codename) {
    let character = DECKERS[codename];

    if (typeof character === "object") {
        character.codename = codename;
        character.money = 0;
        character.mod = {};
        character.def = {
            stats: character.stats,
            hardware: character.hardware
        };

        return character;
    }

    return null;
}

// substract ram and set running software 
export function allocateRam(haap, amount) {
    let decker = haap.has.decker;
    let ram = decker.hardware.ram;
    
    decker.hardware.ram = ram - amount;

    if (decker.hardware.ram <= 0) {
        return false;
    }

    decker.mod.ram = true;

    haap.set({decker: decker});

    return true;
}

// add ram and remove running software 
export function freeRam(haap, amount, code) {
    let decker = haap.has.decker;
    let mod = decker.mod;
    let defRam = decker.def.hardware.ram;
    let newRam = decker.hardware.ram + amount;
    
    decker.hardware.ram = (newRam > defRam) ? defRam : newRam;

    if (newRam === defRam) {
        delete mod.ram;
    }

    delete mod[code];
    decker.mod = mod;

    haap.set({decker: decker});

    return true;
}

export function addIntel(haap, intel) {
    let decker = haap.has.decker;
            
    decker.money = decker.money + intel.value;
    decker.hardware.hdd = decker.hardware.hdd - intel.size;

    haap.set({
        decker: decker
    });

    return true;
}

export function addSoftware(haap, software) {
    let decker = haap.has.decker;
    let available = decker.software;
    let code = software.code;
    let currSoft = available.find(soft => soft['code'] === code);
    let newSoft = {
        code: software.code,
        level: software.level
    };

    if (currSoft) {
        if (currSoft.level > software.level) {
            haap.put('You already have better version of '+software.code+' software', 'warning');
            return false;
        } else {
           // remove code from available software
           decker.software = available.filter(soft => soft['code'] !== code);
        }
    }
    
    decker.software.push(newSoft);
    decker.hardware.hdd = decker.hardware.hdd - software.size;

    haap.set({
        decker: decker
    });

    return true;
}