import { SOFTWARE } from "./Consts";


// check barrier or guard Ice keeping dir or all
export function checkBlocker(node, dir) {
    if (node.ice) {
        var ices = node.ice;
        
        for(let id in ices) {
            var ice = ices[id];
            if ((
                ice.type === "barrier"
                || ice.type === "guard"
            ) && (
                ice.keeps === dir
                || ice.keeps === "all"
            )) {
                 return ice;
            }
        }
    }

    return null;
}

// check Ice keeping node or all
export function checkKeeper(node) {
    if (node.ice) {
        var ices = node.ice;
        
        for(let id in ices) {
            if ((
                ices[id].keeps === 'node'
                || ices[id].keeps === 'all'
            )) {
                 return ices[id];
            }
        }
    }

    return null;
}

// check soldier or killer Ice keeping node or all
export function checkEngage(node, alert = false) {
    if (node.ice) {
        var ices = node.ice;
        
        for(let id in ices) {
            if ((
                ices[id].grade === 'black'
                || (ices[id].grade === 'grey' && alert )
            )) {
                 return ices[id];
            }
        }
    }

    return null;
}

// check if an executed software overpasses this oposition 
export function isOverpass(executed, oposition, oposed) {
    for (let code in executed) {
        let software = SOFTWARE[code];

        if (
            software && 
            software.overpass && 
            software.overpass === oposition &&
            (software.passive || (software.target && executed[code].target === oposed.id)) &&  
            (!oposed.level || executed[code].level > oposed.level)
        ) {
            return true;
        }
    }

    return false;
}


// any black ice or gey ice + alert 
export function checkBlackIce(node, alert =  false) {
    if (node.ice) {
        var ices = node.ice;
        
        for(let id in ices) {
            var ice = ices[id];
            if (
                ice.grade === "black"
                || (
                ice.grade === "grey"
                && alert
            )) {
                console.log(ice);
                 return ice;
            }
        }
    }

    return null;
}
