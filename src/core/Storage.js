export function getStorage(name) {
    var storedState = sessionStorage.getItem(name);

    return JSON.parse(storedState);
}

export function syncStorage(name, newState, object = null) {
    sessionStorage.removeItem(name);

    if (object) {
        for(let id in object) {
            newState[id] = object[id];
        }
    }

    sessionStorage.setItem(name, JSON.stringify(newState));

    return newState;
}
