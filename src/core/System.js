import {AVAILABLE, OPPOSITE} from './Consts'

const DEBUG = false;

export function makeInstance(system) {

    if (DEBUG) console.log('System Json');
    if (DEBUG) console.log(system);
    system["map"] = {};

    system.nodes.forEach(
        function (node) {
            if (DEBUG) console.log('node');
            if (DEBUG) console.log(node);
            node["scripts"] = AVAILABLE[node.type];

            if (DEBUG) console.log('connections');
            node["exits"] = {};
            system.joins.forEach(
                function (way) {
                    if (DEBUG) console.log(way);
                    let via = way.via;
                    if (way.from === node.id) {
                        node["exits"][via] = way.to;
                    }
                    let opposite = OPPOSITE[via];
                    if (way.to === node.id) {
                        node["exits"][opposite] = way.from;
                    }
                }, node
            );
            if (DEBUG) console.log(node.exits);

            if (DEBUG) console.log('ice');
            node["ice"] = {};
            system.ice.forEach(
                function (ice) {
                    if (DEBUG) console.log(ice);
                    if (ice.at === node.id) {
                        node["ice"][ice.id] = ice;
                    }
                }, node
            );
            if (DEBUG) console.log(node.ice);

            system["map"][node.id] = node;
        }
        , system
    );

    if (DEBUG) console.log('Created Map');
    if (DEBUG) console.log(system.map);

    return system;
};

export function removeIce(haap, iceId) {
    let node = haap.has.node;
    
    delete node.ice[iceId];

    haap.set({
        node: node,
        hint: 'Clear-cutted the ICE down'
    });

    return true;
}

