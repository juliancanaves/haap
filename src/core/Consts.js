export const DEFAULT_STATE = {
    hint: 'welcome',
    scene: 'welcome',
    access: null,
    decker: null,
    plugged: false,
    blocker: null,
    engage: null,
    alert: null,
    system: null,
    node: null,
    executed: null,
    map: null,
    endgame: false
};

export const OPPOSITE = {
    n:   's',
    s:   'n',
    e:   'w',
    w:   'e',
    ne:  'sw',
    nw:  'se',
    se:  'nw',
    sw:  'ne',
    nne: 'ssw',
    nnw: 'sse',
    sse: 'nnw',
    ssw: 'nne',
    nee: 'sww',
    nww: 'see',
    see: 'nww',
    sww: 'nee'
};

export const DIRECTIONS = {
    n:   {name: 'north', oposite: 's'},
    s:   {name: 'south', oposite: 'n'},
    e:   {name: 'east', oposite: 'w'},
    w:   {name: 'west', oposite: 'e'},
    ne:  {name: 'north-east', oposite: 'sw'},
    nw:  {name: 'north-west', oposite: 'se'},
    se:  {name: 'south-east', oposite: 'nw'},
    sw:  {name: 'south-west', oposite: 'ne'},
    nne: {name: 'north-north-east', oposite: 'ssw'},
    nnw: {name: 'north-north-west', oposite: 'sse'},
    sse: {name: 'south-south-east', oposite: 'nnw'},
    ssw: {name: 'south-south-west', oposite: 'nne'},
    nee: {name: 'north-east-east', oposite: 'sww'},
    nww: {name: 'north-west-west', oposite: 'see'},
    see: {name: 'south-east-east', oposite: 'nww'},
    sww: {name: 'south-west-west', oposite: 'nee'}
};

export const SCRIPTS = {
    lock:     {
        canvas: 'hack/lock'
    },
    unlock:   {
        canvas: 'hack/unlock'
    },
    delete:   {
        canvas: 'hack/delete'
    },
    /*
    edit:     {
        canvas: 'hack/edit'
    },
    */
    list:     {
        canvas: 'hack/list'
    },
    read:     {
        canvas: 'hack/read'
    },
    download: {
        canvas: 'hack/download'
    },
    message:  {
        canvas: 'hack/message'
    },
    hijack:   {
        canvas: 'hack/hijack'
    },
    sensors:  {
        canvas: 'hack/sensors'
    },
    alert:    {
        canvas: 'hack/alert'
    },
    goto:     {
        canvas: 'hack/goto'
    },
    map:      {
        canvas: 'hack/map'
    },
    shutdown: {
        canvas: 'hack/shutdown'
    },
};

export const AVAILABLE = {
    san : ['lock', 'unlock'],
    spu : [],
    db  : ['list', 'download', 'delete', 'read'], // , 'edit'
    iop : ['message', 'lock', 'unlock'],
    mod : ['hijack', 'sensors'],
    cpu : ['alert', 'goto', 'map', 'shutdown']
};

export const SOFTWARE = {
    attack:   {
        target: 'ice',
        grade: 'grey',
        overpass: 'keeper'
    },
    password: {
        target: 'ice',
        grade: 'white',
        overpass: 'blocker'
    },
    decrypt:  {
        target: 'db'
    },
    stealth:  {
        passive: true,
        overpass: 'blocker'
    },
    shield:   {
        passive: true
    },
    analyzer: {
        target: 'mod'
    },
    repair:   {
    },
    minecraft:   {
        passive: true
    },
    meltdown:   {
        target: 'ice',
        grade: 'black',
        overpass: 'keeper'
    }
};

export const COMMANDS = {
    welcome: {
        hidden: true,
        exclusive: 'unplugged',
        canvas: 'welcome',
        argument: null,
        target: null
    },
    intro: {
        exclusive: 'unplugged',
        canvas: 'intro',
        argument: null,
        target: null
    },
    start: {
        exclusive: 'unplugged',
        target: null
    },
    login: {
        hidden: true,
        exclusive: 'unplugged',
        canvas: 'about'
    },
    account: {
        hidden: true,
        exclusive: 'unplugged',
        canvas: 'about'
    },
    hello: {
        hidden: true,
        canvas: 'simon'
    },
    simon: {
        hidden: true,
        canvas: 'simon'
    },
    help: {
        canvas: 'help',
        output: [
            {
                text: 'More help in `man` pages',
                type: 'message'
            },
        ],
        argument: null,
        target: null
    },
    man: {
        canvas: 'man',
        output: [
            {
                text: 'Read this pages carefully, ',
                type: 'message'
            },
            {
                text: ' the key to complete the game is within it',
                type: 'message'
            },
        ],
        target: null
    },
    access: {
        exclusive: 'unplugged',
        canvas: 'corp/access',
        argument: null,
        target: null
    },
    exits: {
        hidden: true,
        exclusive: 'plugged',
        argument: null,
        target: null
    },
    info: {
        exclusive: 'plugged',
        target: null
    },
    map: {
        exclusive: 'plugged',
        argument: null,
        target: null
    },
    whoami: {
        exclusive: 'plugged',
        canvas: 'simon',
        argument: null,
        target: null
    },
    go: {
        exclusive: 'plugged',
        // canvas: 'node/access_01',
        target: null
    },
    hack: {
        exclusive: 'plugged'
    },
    unload: {
        exclusive: 'plugged',
        target: null
    },
    run: {
        exclusive: 'plugged',
    },
    logout: {
        hidden: true,
        canvas: 'welcome',
        argument: null,
        target: null
    },
    unplug: {
        exclusive: 'plugged',
        canvas: 'effect/sick',
        output: [
            {
                text: 'Back to meat-space',
                type: 'highlight'
            },
        ],
        argument: null,
        target: null
    },
    about: {
        canvas: 'about',
        argument: null,
        target: null
    },

    // aliases for ´start LEVEL´
    easy:   {
        aliasfor: 'start',
        hint: 'start_tutorial',
        canvas: 'corp/metacortex',
        decker: 'neo',
        system: 'metacortex',
        message: 'Access into Metacortex intranet with your employee account'
    },
    medium: {
        aliasfor: 'start',
        hint: 'start_tutorial',
        canvas: 'corp/aztechnology',
        decker: 'dodger',
        system: 'aztechnology',
        message: 'Access into Aztechnology Corp System with fake account'
    },
    hard: {
        aliasfor: 'start',
        hint: 'start_tutorial',
        canvas: 'corp/onosendai',
        decker: 'dixie',
        system: 'onosendai',
        message: 'Access into Ono-Sendai Mainframe overriding authentication'
    },
    cybercorp: {
        aliasfor: 'start',
        hint: 'start_cybercorp',
        canvas: 'corp/cybercorp_denied',
        decker: null,
        system: null,
        warning: 'AuthException "Access denied"'
    },
    cyb32c02p: {
        aliasfor: 'start',
        hint: 'start_cyb32c02p',
        canvas: 'corp/cybercorp',
        decker: 'dixie',
        system: 'cybercorp'
    },

    // aliases for `go DIR` are ['n', 's', 'e', 'w', ...] as DIR
    n:   {aliasfor: 'go', message: 'Heading to north'},
    s:   {aliasfor: 'go', message: 'Heading to south'},
    e:   {aliasfor: 'go', message: 'Heading to east'},
    w:   {aliasfor: 'go', message: 'Heading to west'},
    ne:  {aliasfor: 'go', message: 'Heading to north-east'},
    nw:  {aliasfor: 'go', message: 'Heading to north-west'},
    se:  {aliasfor: 'go', message: 'Heading to south-east'},
    sw:  {aliasfor: 'go', message: 'Heading to south-west'},
    nne: {aliasfor: 'go', message: 'Heading to north-north-east'},
    nnw: {aliasfor: 'go', message: 'Heading to north-north-west'},
    sse: {aliasfor: 'go', message: 'Heading to south-south-east'},
    ssw: {aliasfor: 'go', message: 'Heading to south-south-west'},
    nee: {aliasfor: 'go', message: 'Heading to north-east-east'},
    nww: {aliasfor: 'go', message: 'Heading to north-west-west'},
    see: {aliasfor: 'go', message: 'Heading to south-east-east'},
    sww: {aliasfor: 'go', message: 'Heading to south-west-west'},

    // aliases for `SOFTWARE TARGET` are ['attack', 'password', 'decrypt', 'stealth', ...] as SOFTWARE, argument as TARGET
    attack:   {aliasfor: 'run', message: 'You attempt to overload grey ICE'},
    password: {aliasfor: 'run', message: 'You attempt authenticate yourself to white ICE'},
    decrypt:  {aliasfor: 'run', message: 'You attempt to decrypt a Databank'},
    stealth:  {aliasfor: 'run', message: 'You attempt to turn Stealth mode on'},
    shield:   {aliasfor: 'run', message: 'You attempt to turn Shield mode on'},
    analyzer: {aliasfor: 'run', message: 'You attempt to analyze a Module'},
    repair:   {aliasfor: 'run', message: 'You attempt to repair your hardware'},
    minecraft:{aliasfor: 'run', message: 'You attempt to procatinate'},
    meltdown: {aliasfor: 'run', message: 'You attempt to melt a black ICE'},

    // aliases for `hack SCRIPT` are ['download', 'shutdown', ...] as SCRIPT
    lock:     {aliasfor: 'hack', message: 'You attempt to block access'},
    unlock:   {aliasfor: 'hack', message: 'You attempt to unblock access'},
    delete:   {aliasfor: 'hack', message: 'You attempt to delete a resource'},
    // edit:     {aliasfor: 'hack', message: 'You attempt to edit a resource'},
    read:     {aliasfor: 'hack', message: 'You attempt to read a resource'},
    list:     {aliasfor: 'hack', message: 'You attempt to get list of resources'},
    download: {aliasfor: 'hack', message: 'You attempt to download a resource'},
    message:  {aliasfor: 'hack', message: 'You attempt to send a message'},
    hijack:   {aliasfor: 'hack', message: 'You attempt to gain control of the module'},
    sensors:  {aliasfor: 'hack', message: 'You attempt to read lectures of the module'},
    alert:    {aliasfor: 'hack', message: 'You attempt to silence system alert'},
    goto:     {aliasfor: 'hack', message: 'You attempt to relocate to another node'},
    shutdown: {aliasfor: 'hack', message: 'You attempt to turn off the system mainframe'},

    // alias for man pages
    software: {aliasfor: 'man'},
};

export const NODE_TYPE = {
    san: "the System Access Node",
    spu: "a Sub Processor Unit Node",
    db: "a Databank Node",
    iop: "an Input/Output Port Node",
    mod: "a Module Control Node",
    cpu: "the Central Processor Node"
};

export const DECKERS = {
    default_decker: {
        name: 'Decker name',
        username: '@user1',
        portrait: 'portrait',
        stats: {
            skill: 1,
            health: 1,
            focus: 1
        },
        cyberdeck: 'Radioshack B1',
        hardware: {
            core: 1,
            firewall: 1,
            bus: 1,
            io: 1,
            ram: 10,
            hdd: 100
        },
        software: [
            {code: 'attack', level: 1},
            {code: 'decrypt', level: 1}
        ]
    },
    neo: {
        name: 'Thomas A. Anderson',
        username: 'tanderson',
        portrait: 'neo',
        stats: {
            skill: 2,
            health: 2,
            focus: 2
        },
        cyberdeck: 'IBM Y2017',
        hardware: {
            core: 1,
            firewall: 1,
            bus: 10,
            io: 10,
            ram: 8,
            hdd: 1000
        },
        software: [
            {code: 'password', level: 4},
            {code: 'decrypt', level: 1},
            {code: 'minecraft', level: 9}
        ]
    },
    dodger: {
        name: 'Jerod',
        username: '%username%',
        portrait: 'portrait',
        stats: {
            skill: 3,
            health: 3,
            focus: 3
        },
        cyberdeck: 'Fuji-Hitachi C6',
        hardware: {
            core: 3,
            firewall: 3,
            bus: 30,
            io: 30,
            ram: 9,
            hdd: 3000
        },
        software: [
            {code: 'password', level: 1},
            {code: 'decrypt', level: 3},
            {code: 'attack', level: 4}
        ]
    },
    dixie: {
        name: 'Dixie Flat-line',
        username: '__FS_EXCEPTION__USER_NOT_FOUND__',
        portrait: 'gibson',
        stats: {
            skill: 3,
            health: 3,
            focus: 3
        },
        cyberdeck: 'Ono-Sendai Cyberspace 7',
        hardware: {
            core: 6,
            firewall: 6,
            bus: 60,
            io: 60,
            ram: 10,
            hdd: 6000
        },
        software: [
            {code: 'stealth', level: 3},
            {code: 'attack', level: 5},
            {code: 'decrypt', level: 6},
            {code: 'shield', level: 1}
        ]
    }
};

export const TEXTS = {

    // commands
    welcome: 'Welcome to haap.space',
    hello: 'Greetings',
    simon: 'I am S.I.M.O.N. an Artificial Intelligence',
    intro: 'Enter `start` to start with the tutorials',
    help: 'Enter `about` to know more about the game',
    man: 'Enter `man COMMAND` to see usage of a command',
    about: 'Here you have some links, take your time, I await',
    login: 'request an account by email to haap@doukeshi.org',
    account: 'request an account by email to haap@doukeshi.org',
    start: 'Enter `start LEVEL` or `LEVEL`',
    access_ok: 'Your counciesness is switching to cyberspace',
    start_fail: 'Enter `start LEVEL` or `LEVEL`',
    start_tutorial: 'Enter `access` to connect into the System',
    start_cybercorp: 'You could try `start cyb32c02p`',
    start_cyb32c02p: 'Enter `access` at your own risk',
    start_system: 'Start is only for tutorials. Enter `access SYSTEM`',
    whoami: 'Nice to meet you',
    info: 'Try `go DIRECTION`, `info TARGET`, `hack SCRIPT` or `run SOFTWARE`',
    info_fail: 'Enter `info TARGET` with `node`, `ice` or `DIRECTION`',
    info_white_ice: 'Try running a `password` to overpass it',
    info_grey_ice: 'Try running an `attack` to overpass it',
    info_black_ice: 'Try running away...',
    map: 'Where do we go now',
    go: 'Enter `info` to know about this node',
    go_fail: 'Enter `info DIRECTION` to know about Node connnected in that direction',
    exits: 'Enter `go DIRECTION` to move to that direction',
    offline: 'Enter `start` or try `access SYSTEM` with a known system code',
    exclusive_plugged: 'You are unplugged from cyberspace',
    exclusive_unplugged: 'You are plugged to cyberspace',
    logout: 'Enter `start LEVEL` to try another level',
    unplug: 'Enter `access` to plug again',
    unplug_hint: 'Enter `unplug` to disconnect from the system',
    access_fail: 'Enter `access SYSTEM` with a valid system code',
    system_load_fail: 'Connection refused. Try again in some minutes.',
    black_ice: 'Enter `unplug` while you are still alive',
    run: 'Enter `man software` to know more about in-game software',
    run_fail: 'TARGET in `SOFTWARE TARGET` is the Id of a Node or ICE',
    run_tip: 'More advanced software codes can be found in Databank nodes',
    run_fail_target: 'Enter `SOFTWARE TARGET` with a valid target ID',
    run_fail_node_level: 'Node security level is higher than your intrussion level',
    run_fail_ram: 'Try `unload` software to free Ram',
    run_fail_target_level: 'Target level is greater than yours',
    run_fail_target_ice: 'Enter `info ice` to know about ICE present in the node',
    run_fail_target_ice_grade: 'Enter `info IceId` to know details of a given ICE',
    attack_fail: 'This ICE is more advanced than your software',

    // hack result highlighted message
    hack: 'Enter `man hack` to know more about in-game scripts',
    hack_prevented: 'Your hacking attempt was prevented by an ICE',
    hack_block_overpassed: 'You overpassed the blocking ICE',
    hack_fail: 'Try `hack SCRIPT` with an available script',
    hack_lock: 'Access through this node has been dissabled',
    hack_unlock: 'Access through this node has been enabled',
    hack_delete: 'Resource has been removed from the databank',
    // hack_edit: 'Resource has been altered',
    hack_list: 'List of usable resources in the Databank',
    hack_read: 'Got resource details',
    hack_download: 'Downloaded an usable resource',
    hack_message: 'A message has been emmited',
    hack_hijack: 'Modules controled by this node have been hijacked',
    hack_sensors: 'Sensor lectures have been adjusted',
    hack_alert: 'All alerts has been silented',
    hack_goto: 'Digital representation has been relocated',
    hack_map: 'Command `map` enabled',
    hack_shutdown:  'System server is down! Good job!',
    hack_nothing: 'Nothing happens',

    // software 
    code_shield: 'Firewall of your cyberdeck has been improved',
    code_repair: 'Core of your cyberdeck has been repaired',
    code_decrypt: 'Databank has been decrypted',
    hint_decrypt: 'Enter `hack list´ to see available resources',
    hint_download: 'Enter `hack download RESOURCE´ to get the software/data',
    code_decrypt_fail: 'Software level is not enough to decrypt this databank',
    better_software: 'Try `access marketplace` to get better software',

    // game constraints
    no_map: 'Get the map executing `hack map` at the Cpu node',
    no_hdd: 'Not enough HDD available to store this resource',
    no_ram: 'Not enough RAM, `unload` software',
    nothing: '. . .',


    endgame: 'Send us an screenshot and request your PROMOTION',
};