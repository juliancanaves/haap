import React, { Component } from 'react';
import {fetchAsset} from './core/Fetcher';

const DEBUG = false;

class Canvas extends Component {
  constructor(props) {
      super(props);
      this.state = {
          scene: props.scene,
          content: '<p>Loading . . .</p>',
          map: props.map
      };
  }
  loadScene = (scene, map = null) => {

    if (scene === 'map') {
        let mapArt = (this.state.map) ? this.state.map : map;
        if (mapArt) {
            this.setState({
                content: (mapArt)
            });

            return true;
        } else {
            return false;
        }
    }

    fetchAsset('assets/scenes/'+scene+'.art')
    .then((art) => {
        this.setState({
            content: art,
            scene: scene
        });
    })
    .catch((err) => {
        console.error('Failed to load scene '+scene);
    });
  }
  componentWillReceiveProps(nextProps) {
      if (DEBUG) console.log('nextProps');
      if (DEBUG) console.log(nextProps);
      if (nextProps.scene !==  this.state.scene) {
          this.loadScene(nextProps.scene, nextProps.map);
      }
  }
  componentDidMount() {
      this.loadScene(this.state.scene);
  }
  render() {
    return (
        <div>
            <div className="canvas" dangerouslySetInnerHTML={{__html: this.state.content}} />
        </div>
    );
  }
}

export default Canvas;
