import { COMMANDS } from "../core/Consts";
import { makeCharacter } from '../core/Decker'

var DEBUG = false;

/* 
 * This command:
 *   - sets the system that will be accessed by default
 *   - loads character data on the side
 * 
 *  only tutorials for now
 * 
 */
export default function start (haap, tutorial) {

    if (!tutorial) {
        haap.put('Select your level');
        haap.set({scene: 'start'});

        return false;
    }
    if (DEBUG) console.log('CMD: start tutorial '+tutorial);

    if (!COMMANDS[tutorial]) {
        haap.put('Select your level');
        haap.set({
            hint: 'start_fail',
            scene: 'start',
            access: null,
        });

        return false;
    }

    let data = COMMANDS[tutorial];

    if (data.hint) {
        haap.set({hint: data.hint});
    }

    if (data.message) {
        haap.put(data.message);
    }

    if (data.warning) {
        haap.put(data.warning, 'warning');
        return false;
    }

    haap.set({
        scene: data.canvas,
        access: data.system,
        decker: makeCharacter(data.decker)
    });
}