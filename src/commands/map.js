export default function map (haap) {
    let currNode = haap.has.node;

    if (haap.has.map) {
        haap.set({scene: 'map'});
        haap.outputNodetype(currNode);
    } else {
        haap.put('You dont have a map', 'warning');
        haap.set({hint: 'no_map'});
    }
}