export default function exits (haap) {
    let currNode = haap.has.node;

    haap.reloadOutput(currNode);
    haap.availableExits(currNode);
}
