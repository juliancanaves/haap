import { DIRECTIONS, NODE_TYPE } from "../core/Consts";

const DEBUG = false;

export default function info (haap, target) {
    let currNode = haap.has.node;

    if (DEBUG) console.log(currNode);

    let ices = currNode.ice;
    if (DIRECTIONS[target]) {
        let system = haap.has.system;
        let newNode = system.map[currNode.exits[target]];

        // info node on that direction
        if (!newNode) {
            haap.put('There is no connection to '+DIRECTIONS[target].name+' from this node');
        } else {
            haap.put('The node connected at '+DIRECTIONS[target].name+' is `'+newNode.id+'`, '+NODE_TYPE[newNode.type]);

            // blocker
            for(let id in currNode.ice) {
                if (currNode.ice[id].keeps === target) {
                    haap.put('ICE `'+id+'` is blocking the `'+target+'` direction');
                }
            }
        }
    }
    else if (target === 'node' || !target || (target && currNode.id === target)) {
        haap.infoNode(currNode);
    }
    else if (target === 'entities') {
        // present non-ice entities (human operators)
        haap.put('No other entities in this node, only yours');
    }
    else if (target === 'ice') {
        // present ice
        haap.presentIce(currNode);
        haap.set({hint: 'run', scene: currNode.canvas});
        //@FIXME : dont show if overpassed
    }
    else if (ices[target]) {
        let ice = ices[target];
        if (!ice.hint) {
            ice.hint = 'info_'+ice.grade+'_ice';
        }

        haap.reloadOutput(ice);
        if (ice.grade === 'white') {
            haap.set({blocker: ice});
        } else {
            haap.set({keeper: ice});
        }
    }
    else
    {
        // target is not a valid target
        haap.put('Specified entity `'+target+'` is not here', 'warning');
        // haap.infoNode(currNode);
        haap.set({
            hint: 'info_fail',
            scene: currNode.canvas
        });
    }
}
