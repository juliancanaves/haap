import { TEXTS } from "../../core/Consts";

export default function code (haap, level) {
    let decker = haap.has.decker;
    let defCore = decker.def.hardware.core;
    let newCore = decker.hardware.core + level;

    decker.hardware.core = (newCore > defCore) ? defCore : newCore;

    haap.set({
        decker: decker
    });

    haap.put(TEXTS['code_repair'], 'highlight');
    return true;
}