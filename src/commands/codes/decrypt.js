import { TEXTS } from "../../core/Consts";

export default function code (haap, level, target) {

    var currNode = haap.has.node;

    if (currNode.level && currNode.level <= level) {
        haap.put(TEXTS['code_decrypt_fail'], 'warning');
        haap.set({hint: 'better_software'});
        return false;
    }

    currNode.decrypted = true;

    haap.set({
        hint: 'hint_decrypt',
        node: currNode
    });

    haap.put(TEXTS['code_decrypt'], 'highlight');    
    return true;
}