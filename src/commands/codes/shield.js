import { TEXTS } from "../../core/Consts";

export default function shield (haap, level) {
    var decker = haap.has.decker;

    decker.hardware.firewall = decker.hardware.firewall + level;

    haap.set({
        decker: decker
    });

    haap.put(TEXTS['code_shield'], 'highlight');
    return true;
}