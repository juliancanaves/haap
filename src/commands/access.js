import {makeInstance} from "../core/System";
import {fetchMapServer} from '../core/Fetcher';

var DEBUG = false;

/* 
 * This command:
 *   - loads system map
 *   - sets plugged
 * 
 *  Needs:
 *   - access value (if no argument provided)
 *   - loaded character
 * 
 */

export default function access (haap, syscode) {
    
    if (!syscode && haap.has.access) {
        syscode = haap.has.access;
    }
    
    if (!haap.has.decker) {
        haap.put('Enter `start`, then start a tutorial with `start LEVEL`', 'highlight');
        haap.set({
            hint: 'access_fail',
            system: null,
            scene: 'start'
        });

        return false;
    }

    if (!syscode) {
        haap.put('You must select a tutorial first: `easy`, `medium` and `hard`', 'warning');
        haap.set({
            hint: 'access_fail',
            scene: 'start'
        });

        return false;
    }

    fetchMapServer(syscode)
    .then( (response) => {
        return  makeInstance(response);
    })
    .then((instance) => {
        if (DEBUG) console.log(instance);

        let sanNode = instance.map['san'];
        haap.set({
            plugged: true,
            system: instance,
            node: sanNode,
            hint: 'access_ok'
        });

        haap.put('You have plugged into ' +syscode+' system', 'highlight');
        if (instance.message) {
            haap.put(instance.message);
        }

        setTimeout(
            () => {
                haap.infoNode(sanNode);
                haap.set({hint: 'info'});
            }
            , 2000
        );
    })
    .catch( (err) => {
        haap.put('Failed to load system instance', 'warning');
        haap.put('Try the tutorials first: `easy`, `medium` and `hard`', 'highlight');
        haap.set({
            hint: 'system_load_fail',
            system: null
        });
    });
}