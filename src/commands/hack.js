// import * as logic from '../core/Logic';
import { SCRIPTS, NODE_TYPE, TEXTS } from "../core/Consts";
import { checkKeeper, isOverpass } from '../core/Logic';
import * as hacks from './hacks';

const DEBUG = false;

/*
 * This comand:
 *   - checks the availability of the hack
 *   - check if the hack is blocked by Ice 
 *   - check if the clockage is overpassed by exected software
 *   - 
 */
export default function hack (haap, script, target) {
    let currNode = haap.has.node;

    if (DEBUG) console.log('Current node');
    if (DEBUG) console.log(currNode);

    if (!script) {
        haap.put('Specify a proper `hack SCRIPT`');
        haap.availableScripts(currNode);
        haap.set({hint: 'hack_fail'});

        return false;
    }

    script = script.trim();
    if (DEBUG) console.log('script '+script);

    if (!SCRIPTS[script] || !currNode.scripts.includes(script)) {
        if (DEBUG) console.error('Script is not available');
        haap.put(script+' is not an available script in '+NODE_TYPE[currNode.type]);
        haap.availableScripts(currNode);
        haap.set({hint: 'hack_fail'});

        return false;
    }

    if (DEBUG) console.log('Script is available, check keeper');

    // check Ice preventing hacks
    var keeper = checkKeeper(currNode);
    console.log('keeper');
    console.log(keeper);

    if (keeper) {
        // executed Software could allow Overpassing this ICE keep
        let overpass = isOverpass(haap.has.executed, 'keeper', keeper);

        if (!overpass) {
            haap.reloadOutput(keeper);

            // this ice becomes actual keeper (until character move to another node or overpass it)
            haap.set({keeper: keeper});

            return false; 
        }
    }

    haap.set({scene: 'hack/empty_hands'});

    // briefly show empty hands
    setTimeout(
        () => {
            haap.set({
                scene: (SCRIPTS[script].canvas) ? SCRIPTS[script].canvas : currNode.canvas
            });

            // keep hacking hands for a while
            setTimeout(
                () => {
                    haap.set({scene: currNode.canvas});
                    if (hacks[script](haap, target)) {
                        haap.put(TEXTS['hack_'+script]);
                    }
                }, 1000
            );
        }, 500
    );

    return true;
}