import { COMMANDS } from '../core/Consts';

export default function man (haap, command) {
    if (command && COMMANDS[command]) {
        haap.set({scene: 'man/man_'+command});
    } else {
        haap.set({scene: 'help'});
        haap.availableCommands();
    }
}