import { DIRECTIONS } from '../core/Consts';
import { checkBlocker, checkEngage, isOverpass } from '../core/Logic';
import { setTimeout } from 'timers';

export default function go (haap, dir) {

    let currNode = haap.has.node;
    let system   = haap.has.system;
    let exits    = currNode.exits;

    // command without argument
    if (!dir) {
        haap.put('Please execute `go DIRECTION` with a proper DIRETION', 'warning');
        haap.set({hint: 'go_fail'});
        haap.availableExits(currNode);

        return false;
    }

    dir = dir.trim();
    
    // dir is not a direction
    if (!DIRECTIONS[dir]) {
        haap.put(dir+' is not a DIRECTION', 'warning');
        haap.set({hint: 'go_fail'});
        haap.availableExits(currNode);

        return false;
    }

    if (!exits[dir]) {
        haap.put('No connection to '+DIRECTIONS[dir].name+' from this node', 'warning');
        haap.set({hint: 'go_fail'});
        haap.availableExits(currNode);

        return false;
    }


    // : ice, 'block_'+action
    
    
    
    // ICE could be blocking that dir
    var blocker = checkBlocker(currNode, dir);

    if (blocker) {
        // executed Software could allow Overpassing this ICE blockage
        let overpass = isOverpass(haap.has.executed, 'blocker', blocker);

        if (!overpass) {
            haap.put(blocker.type+' ICE `'+blocker.id+'` is blocking '+DIRECTIONS[dir].name+' `'+dir+'` exit', 'warning');
            haap.reloadOutput(blocker);

            // this ice becomes actual blocker (until character go to another direction or find another blocker)
            haap.set({blocker: blocker});

            return false; 
        }
        
        haap.put('You now `go '+dir+'` through ICE `'+blocker.id+'`');
        haap.set({
            blocker: null,
            keeper: null,
            scene: (blocker.canvas_open) ? blocker.canvas_open : currNode.canvas
        });

    } else {
        // move to another node throug non blocked exit
        haap.set({blocker: null, keeper: null});
    }
    
    // @TODO : the System itself may do additional constraint (such as grade of security vs decker skill...)

    setTimeout(
        // move
        () => {
            // connection from this node through that direction
            let newNode = system.map[exits[dir]];

            // @TODO : any avoidance to get into the newNode must show a warning ("You shall not pass")
            // such a black ice with active alarm, which will jump to current node, engage and prevent decker to move

            haap.infoNode(newNode);    
            haap.set({
                node: newNode,
                alert: (newNode.type === 'cpu') // @TODO : alert can be raised in many other circumstances
            });

            // Ice awaiting in next node
            setTimeout(
                () => {
                    // check if there is Black Ice or a Grey Ice active by alert
                    var engaged = checkEngage(newNode, haap.has.alert);
        
                    if (engaged) {
                        haap.put(engaged.type+' ICE `'+engaged.id+'` is active and engaged');
                        haap.set({
                            engage: engaged,
                            blackIce: (engaged.grade === 'black')
                        });
                        haap.reloadOutput(engaged);
                    }
                },
                500
            );
        },
        200
    );

    return true;
}