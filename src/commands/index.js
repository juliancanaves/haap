// all runable commands must be in this index
export {default as access} from './access';     
export {default as exits} from './exits';      
export {default as go} from './go';    
export {default as hack} from './hack';  
export {default as help} from './help';
export {default as info} from './info';
export {default as logout} from './logout';
export {default as man} from './man';
export {default as map} from './map';
export {default as run} from './run';
export {default as start} from './start';        
export {default as unload} from './unload';
export {default as unplug} from './unplug';
export {default as whoami} from './whoami';
