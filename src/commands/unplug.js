import { DEFAULT_STATE } from '../core/Consts';
import { syncStorage } from '../core/Storage';

/*
 * this command removes plugged state and resets to Default State
 */
export default function unplug (haap) {
    var offHaapState = DEFAULT_STATE;    
    
    offHaapState.hint = 'unplug';
    offHaapState.scene = haap.has.system.canvas; // keep corporation view 
    offHaapState.access = haap.has.access; // keep system on pre-access
    offHaapState.decker = haap.has.decker; // same stats (gradually recover)
    offHaapState.map = haap.has.map; // keeps the map

    syncStorage('haap', offHaapState);

    console.error(offHaapState);

    haap.set(offHaapState);
}