import { SOFTWARE } from "../core/Consts";
import { freeRam } from '../core/Decker';

const DEBUG = true;

export default function unload (haap, code) {

    // basic command checks
    if (!code) {
        haap.put('Must specify a running software Code to `unload`', 'warning');
        return false;
    }
    
    let decker = haap.has.decker;
    let available = decker.software;

    let softCode = available.find(soft => soft['code'] === code);
    if (!SOFTWARE[code] || !softCode) {
        return false;
    }

    if (!SOFTWARE[code].passive) {
        haap.put('`'+code+'` is not consuming RAM, no need to `unload`');
        return false;
    }

    // check if this code is within the decker modified values
    let executed = (haap.has.executed) ? haap.has.executed : {};
    
    if (DEBUG) console.log('executed');
    if (DEBUG) console.log(executed);

    if (!executed[code]) {
        haap.put('`'+code+'` is not active');
        return false;
    }

    let softLevel = softCode['level'];

    if (freeRam(haap, softLevel, code)) {
        delete executed[code];
        haap.set({executed: executed});
        haap.put(softLevel+' GiB of RAM has been freed', 'highlight');
    } else {
        console.error('Decker::allocateRam failed');
        return null;
    }

    return true;
}
