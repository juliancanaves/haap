import { SOFTWARE, NODE_TYPE } from '../core/Consts';
import * as codes from './codes';
import { allocateRam } from '../core/Decker';
import { removeIce } from '../core/System';

var DEBUG = false;

export default function run (haap, code, target) {
    haap.set({hint: 'run'});

    // basic command checks
    if (!code) {
        haap.put('Must specify a software Code to `run`', 'warning');
        return false;
    }

    let decker = haap.has.decker;
    var available = decker.software;

    if (!SOFTWARE[code]) {
        haap.put(code+' is not a software', 'warning');
        haap.availableSoftware(available);
        return false;
    }

    let software = SOFTWARE[code]; 

    // check if this code is within the decker software
    let softCode = available.find(soft => soft['code'] === code);
    if (!softCode) {
        haap.put('You dont have this software', 'warning');
        return false;
    }
    
    let softLevel = softCode['level'];
    let targetedIce = null;

    // check ram
    let ram = decker.hardware.ram;

    if (softLevel > ram) {
        haap.put('You dont have enough ram to run this software', 'warning');
        haap.put('You need at least '+softLevel+' free ram');
        haap.set({hint: 'run_fail_ram'});
        return false;
    }

    // passive software running or active software executed versus target
    var executed = (haap.has.executed) ? haap.has.executed : {};

    // passive software
    if (software.passive) {
        // check software executed
        if (executed[code]) {
            haap.put('An instance of `'+code+'` is already running', 'warning');
            return false;
        }

        if (allocateRam(haap, softLevel)) {
            haap.put('`'+code+'` is now running, allocated '+softLevel+' GiB of RAM', 'highlight');
        } else {
            console.error('Decker::allocateRam failed');
            return null;
        }

    } else {
        // check valid target 

        let ice = haap.has.system.ice;
        let currNode = haap.has.node;

        // ice target
        if (software.target === 'ice') {
            if (!target) {
                haap.put(code+' requires a valid target', 'warning');
                haap.set({hint: 'run_fail_target'});
                return false;
            }

            let targetIce = ice.find(i => i['id'] === target);
            if (!targetIce) {
                haap.put(target+' is not an ICE Id', 'warning');
                haap.set({hint: 'run_fail_target_ice'});
                return false;
            } else if (software.grade && targetIce.grade !== software.grade) {
                haap.put('`'+code+'` is only effective versus '+software.grade+' ICE', 'warning');
                haap.set({hint: 'run_fail_target_ice_grade'});
                return false;
            }
        }

        // node target
        if (software.target && !target) {
            if (software.target !== currNode.type) {
                haap.put('You are not in '+NODE_TYPE[software.target], 'warning');
                haap.set({hint: 'run_fail_target'});
                return false;
            } else {
                target = currNode.id;
            }
        }

        if (software.target) {
            haap.put('Executing `'+code+'` against `'+target+'` '+software.target, 'highlight');

            // check overpass before being blocked
            if (currNode.ice[target]) {
                targetedIce = currNode.ice[target];
            }
        }
    }
    
    executed[code] = {
        code: code,
        level: softLevel,
        target: target
    };
    haap.set({executed: executed});

    if (DEBUG) console.log('executed');
    if (DEBUG) console.log(executed);

    haap.set({scene: 'effect/empty_hands'});

    // briefly show empty hands
    setTimeout(
        () => {
            // SOFTWARE HANDLER
            if (typeof codes[code] === "function") {
                codes[code](haap, softLevel, target);
            }
            
            haap.set({
                scene: (SOFTWARE[code].canvas) ? SOFTWARE[code].canvas : 'effect/execute'
            });

            // keep hacking hands for a while
            setTimeout(
                () => {
                    haap.set({scene: haap.has.node.canvas});
                    checkOverpass(haap, 'blocker', executed[code], targetedIce);
                    checkOverpass(haap, 'keeper', executed[code], targetedIce);
                }, 1000
            );
        }, 500
    );

    return true;
}

function checkOverpass(haap, oposition, software, targetedIce = null) {
    if (DEBUG) console.log(oposition);
    
    let ice = (targetedIce) ? targetedIce : haap.has[oposition];
    if (DEBUG) console.log(ice);

    if (ice) {
        let deOpose = {
            scene: (ice.canvas_open) ? ice.canvas_open : haap.has.node.canvas
        }; 
        deOpose[oposition] = null;

        let code = software.code;

        if (
            SOFTWARE[code].overpass && 
            SOFTWARE[code].overpass === oposition
        ) {
            if (software.level > ice.level || !ice.level) {
                haap.put('Your `'+code+'` allows you to overpass `'+ice.id+'`', 'highlight');

                if (haap.has.engage && haap.has.engage.id === ice.id ) {
                    deOpose['engage'] = null;
                }

                haap.set(deOpose);
                removeIce(haap, ice.id);
            } else {
                haap.put('Your `'+code+'` level is not enough to overpass `'+ice.id+'`', 'warning');
                haap.set({scene: ice.canvas});
            }
        }
    }
}