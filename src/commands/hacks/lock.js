export default function lock (haap) {
    var currNode = haap.has.node;
    currNode.lock = true;
    
    haap.set({
        node: currNode
    });

    return true;
}