export default function unlock (haap) {
    var currNode = haap.has.node;
    currNode.lock = false;
    
    haap.set({
        node: currNode
    });

    return true;
}