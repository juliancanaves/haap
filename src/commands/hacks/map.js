import {fetchMapServer} from '../../core/Fetcher';
import { TEXTS } from "../../core/Consts";

export default function map (haap) {

    let syscode = haap.has.access;

    fetchMapServer(syscode+'/map', true)
    .then( (response) => {
        console.log('map responded');
        haap.set({
            scene: 'map',
            map: response
        });
        haap.put(TEXTS['hack_map'], 'highlight');
    })
    .catch( (err) => {
        console.error('ERROR fetching map: '+err);
        haap.set({
            hint: 'load_map_fail',
            map: null
        });
        haap.put(err, 'warning');
    });
}