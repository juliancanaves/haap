export default function hijack (haap) {
    var currNode = haap.has.node;
    currNode.hijack = true;
    
    haap.set({
        node: currNode
    });

    return true;
}