import { isDecrypted, hasResources, isResource, isEnoughSpace, consoleDownloadResource, getResource  } from './db-hacks';
import unload from '../unload';
import { addSoftware, addIntel } from '../../core/Decker';

export default function download (haap, code) {

    if (isDecrypted(haap)
     && hasResources(haap)
     && isResource(haap, code)
    ) {
        consoleDownloadResource(haap, code);

        var decker = haap.has.decker;
        var resource = getResource(haap, code);

        if (isEnoughSpace(haap, resource, decker)) {
            haap.put('Downloaded complete');
            switch(resource.type) {
                case 'data':
                    if (!resource.vlaue) {
                        resource.value = resource.size;
                    }
                    
                    addIntel(haap, resource);
                    haap.set({hint: 'Sells for '+resource.value+' ¥'});
        
                    break;

                case 'software':
                    // unload first
                    unload(haap, code);    
                    addSoftware(haap, resource);
                    haap.put('You can now `run '+resource.code+'`', 'highlight');
        
                    break;
                default:
            }

            return true;
        }
    }

    return false;
}