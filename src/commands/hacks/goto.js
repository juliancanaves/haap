export default function goto (haap, nodeId) {
    var system   = haap.has.system;

    if (!system.map[nodeId]) {
        haap.put('Destination `'+nodeId+'` is not a node in this system', 'warning');
        return false;
    }

    var newNode = system.map[nodeId];
    haap.infoNode(newNode);    
    haap.set({
        node: newNode
    });

    return true;
}