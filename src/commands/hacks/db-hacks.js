export function isDecrypted(haap) {
    try {
        if (!haap.has.node.decrypted) {
            haap.put('Data is encrypted', 'warning');
            return false;
        }
    } catch (error) {
        console.error(error);
        return null;
    }
    return true;
}

export function hasResources(haap) {
    try {
        if (!haap.has.node.resources) {
            haap.put('There is any usable resource');
            return false;
        }
    } catch (error) {
        console.error(error);
        return null;
    }
    return true;
}

export function listResources(haap) {
    try {
        var resources = haap.has.node.resources;
        let list = '';
        for(let name in resources) {
            list = list+' '+name+',';
        }
        haap.put('Usable resources are:'+list.substr(0, list.length-1), 'highlight');
    } catch (error) {
        console.error(error);
        return null;
    }
    return true;
}

export function isResource(haap, code) {
    try {
        var resources = haap.has.node.resources;
        if (!resources[code]) {
            haap.put('Requested resource `'+code+'` not found');
            return false;
        }
    } catch (error) {
        console.error(error);
        return null;
    }
    return true;
}

export function getResource(haap, code) {
    try {
        var resource = haap.has.node.resources[code];
    } catch (error) {
        console.error(error);
        return null;
    }
    return resource;
}

export function isEnoughSpace(haap, resource, decker) {
    try {
        if (decker.hardware.hdd < resource.size) {
            haap.put('You dont have enough space on your HDD', 'warning');
            return false;
        }
    } catch (error) {
        console.error(error);
        return null;
    }
    return true;    
}

export function consoleReadResource(haap, code) {
    try {
        var resource = haap.has.node.resources[code];
        haap.put('Resource `'+code+'` is '+resourceTxt(resource), 'highlight');
        if (resource.description) {
            haap.put(resource.description);
        }
    } catch (error) {
        console.error(error);
        return null;
    }
    return true;
}

export function consoleDownloadResource(haap, code) {
    try {
        var resource = haap.has.node.resources[code];
        haap.put('Downloading '+resourceTxt(resource));
        if (resource.description) {
            haap.put(resource.description);
        }
    } catch (error) {
        console.error(error);
        return null;
    }
    return true;
}

function resourceTxt (resource) {
    var txt = resource.type;

    if (resource.type === 'software') {
        txt += ' '+resource.code+' ['+resource.level+']';
    }

    if (resource.size) {
        txt += ' intel ('+resource.size+' GiB)';
    }

    return txt;
}

// remove this resource from the databank node
export function deleteResource(haap, code) {
    delete haap.has.node.resources[code];
    return true;
}