import { isDecrypted, hasResources, listResources  } from './db-hacks';

export default function db_list (haap) {
    if (
        isDecrypted(haap) &&
        hasResources(haap)
    ) {
        listResources(haap);
        haap.set({hint: 'hint_download'});
        return true;
    }

    return false;
}