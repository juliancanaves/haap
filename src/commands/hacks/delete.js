import { hasResources, isResource, deleteResource  } from './db-hacks';

export default function bd_delete (haap, code) {

    if (
        hasResources(haap) && 
        isResource(haap, code)
    ) {
        deleteResource(haap, code);
        return true;
    }

    return false;
}