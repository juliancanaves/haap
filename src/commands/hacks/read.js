import { isDecrypted, hasResources, isResource, consoleReadResource  } from './db-hacks';

export default function db_read (haap, code) {

    if (
        isDecrypted(haap) && 
        hasResources(haap) && 
        isResource(haap, code)
    ) {
        consoleReadResource(haap, code);
        return true;
    }

    return false;
}