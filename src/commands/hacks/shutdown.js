import unplug from '../unplug';
import { COMMANDS } from '../../core/Consts';

export default function shutdown (haap) {
    unplug(haap);
    haap.set({
        hint: 'hack_shutdown',
        scene: 'corp/mainframe_01',
        access: null
    });

    if (haap.has.access === 'onosendai') {

        setTimeout(
            () => {
                let data = COMMANDS['cybercorp'];

                if (data.hint) {
                    haap.set({hint: data.hint});
                }
            
                if (data.message) {
                    haap.put(data.message);
                }
            
                if (data.warning) {
                    haap.put(data.warning, 'warning');
                }
            
                haap.set({
                    scene: data.canvas,
                    access: data.system
                });
            }, 1000
        );


    }

    return true;
}