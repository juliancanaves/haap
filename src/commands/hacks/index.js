// all runable commands must be in this index
export {default as lock} from './lock';     
export {default as unlock} from './unlock';     
export {default as delete} from './delete';     
// export {default as edit} from './edit';     
export {default as read} from './read';     
export {default as list} from './list';     
export {default as download} from './download';     
export {default as message} from './message';     
export {default as hijack} from './hijack';     
export {default as sensors} from './sensors';     
export {default as alert} from './alert';     
export {default as goto} from './goto';     
export {default as map} from './map';     
export {default as shutdown} from './shutdown';
