import { DEFAULT_STATE } from '../core/Consts';
import { syncStorage } from '../core/Storage';

/*
 * this command removes all state and resets to Default State
 */
export default function logout (haap) {
    var blankHaapState = DEFAULT_STATE;

    blankHaapState.hint = 'offline';
    blankHaapState.scene = 'about';
    blankHaapState.profile = null;
    syncStorage('haap', blankHaapState);

    console.error(blankHaapState);
    haap.set(blankHaapState);
}