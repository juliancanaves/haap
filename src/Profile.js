import React, { Component } from 'react';
import {fetchAsset} from './core/Fetcher';

const DEBUG = false;

class Portrait extends Component {
    constructor(props) {
        super(props);

        this.state = {
            portraitFile: this.props.portrait,
            portrait: ''
        };
    }
    loadPortrait(portrait) {
        fetchAsset('assets/portraits/'+portrait+'.art')
        .then((art) => {
            this.setState({
                portraitFile: portrait,
                portrait: art
            });
        })
        .catch((err) => {
            console.error('Failed to load portrait:'+portrait);
        });
    }        
    componentDidMount() {
        this.loadPortrait(this.state.portraitFile);
    }
    componentWillReceiveProps(nextProps) {
        if (DEBUG) console.log('portrait will receive props. Now portraitFile: '+this.state.portraitFile+' portrait:'+this.state.portrait.substr(0, 10));
        if (DEBUG) console.log(nextProps);
        if (nextProps.portrait !== this.state.portraitFile) {
            this.loadPortrait(nextProps.portrait);
        }
    }
    render() {
        return (
            <div className="portrait">{this.state.portrait}</div>
        )
    }
}

function Stats (props) {
    let stats = props.stats;
    return (
        <div className="stats">
            <fieldset>
                <dl>
                    <dt>Skill</dt><dd>{stats.skill}</dd>
                    <dt>Health</dt><dd>{stats.health}</dd>
                    <dt>Focus</dt><dd>{stats.focus}</dd>
                </dl>
            </fieldset>
        </div>
    )
}

function Software (props) {
    return (
        <div className="software">
            <fieldset>
                <legend>Software</legend>
                <dl>
                    {props.software.map(function (soft) {
                        return [
                            <dt>{soft.code}</dt>,
                            <dd>{soft.level}</dd>
                        ];
                    })}
                </dl>
            </fieldset>
        </div>
    )
}

function Hardware (props) {
    let hardware = props.hardware;
    return (
        <div className="hardware">
            <fieldset>
                <legend>Hardware</legend>
                <dl>
                    <dt>Core</dt><dd>{hardware.core}</dd>
                    <dt>Firewall</dt><dd>{hardware.firewall}</dd>
                    <dt>Bus</dt><dd>{hardware.bus}</dd>
                    <dt>I/O</dt><dd>{hardware.io}</dd>
                    <dt className="ram">RAM</dt><dd>{hardware.ram}</dd>
                    <dt className="hdd">HDD</dt><dd>{hardware.hdd}</dd>
                </dl>
            </fieldset>
        </div>
    );
}

class Profile extends Component {
    constructor(props) {
        super(props);

        this.state = {
            text: '',
            decker: (props.decker)
        };
    }
    loadWelcome() {
        fetchAsset('assets/pages/side_welcome.txt')
        .then((art) => {
            this.setState({
                text: art
            });
        })
        .catch((err) => {
            console.error('Failed to load Side Welcome page');
        });
    }        
    componentDidMount() {
        if (this.state.decker === null) {
            this.loadWelcome()
        }
    }
    componentWillReceiveProps(nextProps) {
        if (DEBUG) console.log('profile will receive props. Now decker: '+this.state.decker);
        if (DEBUG) console.log(nextProps);

        if (nextProps.decker !==  this.state.decker) {
            this.setState({
                decker: nextProps.decker
            });

            if (nextProps.decker === null) {
                this.loadWelcome()
            }

        } else {
            if (DEBUG) console.log('profile is same');
        }
    }
    render() {
        if (this.state.decker) {
            let decker = this.state.decker;
            return (
                <div className="profile">
                    <div className="name">{decker.name}</div>
                    <section className="decker">
                        <Portrait portrait={decker.portrait} />
                        <Stats stats={decker.stats}/>
                    </section>
                    <section className="cyberdeck">
                        <div className="name">{decker.cyberdeck}</div>
                        <Hardware hardware={decker.hardware}/>
                        <Software software={decker.software}/>
                    </section>
                </div>
            );
        } else {
            return (
                <div className="profile" dangerouslySetInnerHTML={{__html: this.state.text}} />
            );
        }
    }
}

export default Profile;