import React, { Component } from 'react';
import { TEXTS } from './core/Consts';

class Hint extends Component {
    constructor(props) {
        super(props);
        this.state = {
            hint: props.hint
        };
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.hint !==  this.state.hint) {
            this.setState({
                hint: nextProps.hint
            });
        }
    }
    render() {
        let text = (TEXTS[this.state.hint] !== undefined) ? TEXTS[this.state.hint] : this.state.hint;
        return (
            <div className="hint"><span className="simon">Simon says :</span><span className="text">{text}</span></div>
        );
    }
}

export default Hint;