import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Command from './Command';
import { syncStorage, getStorage } from './core/Storage';

const FONT_WIDTH = 7.5;
const DEBUG = false;
const PREFIXES = {
    message: '>',
    highlight: '~',
    warning: '!',
    command: '$',
};

class Terminal extends Component{
    constructor(props) {
        super(props);

        this.haapState = props.haapState;

        this.state = {
            input: '',
            output: [
                {content: 'Ready:', type: 'message'}
            ],
            history: []
        };

        var storedState = getStorage('terminal');
        this.state = (storedState) ? storedState : syncStorage('terminal', this.state);

        this.commandHandler = new Command(
            {
                addOutput: this.addOutput,
                stateHandler: props.stateHandler,
                haapState: props.haapState
            }
        );
    }
    updateHistory = (history) => {
        let newHistory = { history: history };
        this.setState(newHistory);
        syncStorage('terminal', this.state, newHistory);
    }
    addHistory = (entry) => {
        let history = this.state.history;
        history.push(entry);
        if (DEBUG) console.log('history');
        if (DEBUG) console.log(history);
        this.updateHistory(history);
    }
    updateOutput = (output) => {
        let newOutput = { output: output };
        this.setState(newOutput);
        syncStorage('terminal', this.state, newOutput);
    }
    clearOutput = () => {
        this.updateOutput([{content: 'Ready:', type: 'message'}]);
    }
    addOutput = (content, type) => {
        if (type === null || type === undefined) {
            type = 'message';
        }
        let output = this.state.output;
        output.push({'content': content, 'type': type});
        this.updateOutput(output);
    }
    clearInput = () => {
        this.textInput.value = null;
        this.textInput.style.width = '0px';
    }
    parseInput = () => {
        let input = this.textInput.value.toLowerCase();
        if (DEBUG) console.log('Input: '+input);
        let commands = input.split(';');
        commands.forEach(this.processCommand);
    }
    processCommand = (command) => {
        if (DEBUG) console.log('Command: '+command);
        this.addOutput(command, 'command');
        this.addHistory(command);
        let args = command.trim().split(' ');
        if (args[0] === 'clear') {
            this.clearOutput();
        } else {
            this.commandHandler.updateHaapState(this.haapState);
            this.commandHandler.attempt(...args);
        }
    }
    handleInput = (e) => {
        // @TODO : up and down arrows to use output
        if (e.key === "Enter") {
            this.parseInput();
            this.clearInput();
        } else if (e.key === "Backspace") {
            this.textInput.style.width = ((this.textInput.value.length - 1) * FONT_WIDTH) + 'px';
        } else {
            this.textInput.style.width = ((this.textInput.value.length + 1) * FONT_WIDTH) + 'px';
        }
    }
    componentDidMount() {
        this.textInput.focus();
    }
    componentDidUpdate() {
        let node = ReactDOM.findDOMNode(this);
        node.scrollTop = node.scrollHeight
    }
    componentWillReceiveProps(nextProps) {
        this.haapState = nextProps.haapState;
    }
    render() {
        const output = this.state.output.map(function(op, i) {
            return <div key={i} className={op.type}><div width="100%"><span>{PREFIXES[op.type]} {op.content}</span></div></div>
        });
        // @TODO : do the caret as css animation on the input::after
        return (
            <div className="terminal">
                <div className="terminal-output">
                    {output}
                </div>
                <div className="cmd enabled">
                    <span className="prompt">$&nbsp;</span>
                    <input type="text" autoFocus
                           ref={(input) => { this.textInput = input; }}
                           onBlur={() => {this.textInput.focus()}}
                           onKeyDown={this.handleInput}
                           style={{width: '0px'}}
                    />
                    <span className="cursor blink">&nbsp;</span>

                </div>
            </div>
        )
    }
}

export default Terminal;
