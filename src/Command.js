import { Component } from 'react';
import {COMMANDS, NODE_TYPE} from './core/Consts';
import * as commands from './commands';

const DEBUG = false;

class Command extends Component {
    constructor(props) {
        super(props);

        // terminal output handler
        this.put = props.addOutput;

        // Haap state handler
        this.set = props.stateHandler;

        // current Haap State
        this.has = props.haapState;
    }

    // updateHaapState for the Command instance
    updateHaapState = (haapState) => {
        this.has = haapState;
    }

    // internal methods
    infoNode = (node) => {
        this.outputNodetype(node);
        this.reloadOutput(node);
        this.presentIce(node);
        this.availableExits(node);
    }

    reloadOutput = (item) => {
        if (item.hint !== undefined) {
            this.set({hint: item.hint});
        }

         // @FIXME : canvas depending on overpass
        if (item.canvas !== undefined) {
            this.set({scene: item.canvas});
        }

        if (item.message !== undefined) {
            this.put(item.message);
        }
        if (item.highlight !== undefined) {
            this.put(item.highlight, 'highlight');
        }
        if (item.warning !== undefined) {
            this.put(item.warning, 'warning');
        }
    }

    outputNodetype = (node) => {
        this.put('You are in '+NODE_TYPE[node.type]);

        if (node.lock) {
            this.put('Access through this node is dissabled');
        }

        if (node.hijack) {
            this.put('Modules controled by this node are hijacked');
        }
    }

    availableCommands = () => {
        let list = '';
        for(let cmd in COMMANDS) {
            if (COMMANDS[cmd].aliasfor === undefined
                && COMMANDS[cmd].hidden !== true
                && (
                    COMMANDS[cmd].exclusive === undefined
                    || (COMMANDS[cmd].exclusive === 'plugged' && this.has.plugged === true)
                    || (COMMANDS[cmd].exclusive === 'unplugged' && this.has.plugged === false)
                )
            ) {
                list = list+' '+cmd+',';
            }
        }
        this.put('Available commands are:'+list.substr(0, list.length-1), 'highlight');
    }

    availableExits = (node) => {
        let list = '';
        for(let way in node.exits) {
            list = list+' '+way+',';
        }
        this.put('Available DIRECTIONs are:'+list.substr(0, list.length-1), 'highlight');
    }

    availableScripts = (node) => {
        if (!node.scripts || node.scripts.length === 0) {
            this.put('There are no SCRIPTs available in '+NODE_TYPE[node.type], 'warning');
        } else {
            let list = '';
            for(let script in node.scripts) {
                list = list+' '+node.scripts[script]+',';
            }
            this.put('Available SCRIPTs are:'+list.substr(0, list.length-1), 'highlight');
        }
    }

    availableSoftware = (software) => {
        let list = '';
        for(let i in software) {
            list = list+' '+software[i].code+',';
        }
        this.put('Available CODEs are:'+list.substr(0, list.length-1), 'highlight');
    }

    presentIce = (node) => {
        if (node.ice) {
            for(let id in node.ice) {
                if (node.ice[id].inspect) {
                    this.put('ICE `'+id+'` is present. '+node.ice[id].inspect);
                } else {
                    this.put('ICE `'+id+'` is present.');
                }
            }
        }
    }

    iceEngaged = () => {
        let engaged = this.has.engage;
        if (engaged) {
            this.put('ICE `'+engaged.id+'` is engaged!!', 'warning');
            this.set({scene: engaged.canvas});
            this.set({hint: engaged.hint});
        }
    }

    outputThreads = () => {
        this.iceEngaged();

        if (this.has.alert) {
            this.put('System is under alert!', 'warning');
        }
    }

    attempt(command, argument, target, flag) {
        if (DEBUG) console.log('Haap received. Command:'+command+' Argument:'+argument+' Target:'+target);
        if (DEBUG) console.log(this.has);

        // maybe alias for another command
        let alias = null;
        if (COMMANDS[command] && COMMANDS[command].aliasfor) {
            alias = COMMANDS[command].aliasfor;
            if (DEBUG) console.log('input '+command+' is alias for '+alias);
            target = argument;
            argument = command;
            command = alias;
            if (DEBUG) console.log('Aliased. Command:'+command+' Argument:'+argument+' Target:'+target);
        }

        // in command list
        if (COMMANDS[command]) {
            let cmd = COMMANDS[command];

            // must be pluged for this command
            if (cmd.exclusive === 'plugged' && this.has.plugged !== true) {
                this.put('Must `access` before entering this command', 'warning');
                this.set({hint: 'exclusive_plugged'});
                return false;
            }

            // must be unplugged for this command
            if (cmd.exclusive === 'unplugged' && this.has.plugged !== false) {
                this.put('Must `unplug` before entering this command', 'warning');
                this.set({hint: 'exclusive_unplugged'});
                return false;
            }

            // nullifies
            if (cmd['argument'] === null) {
                if (DEBUG) console.log('Argument nullified. was: '+argument);
                argument = null;
            }
            if (cmd['target'] === null) {
                if (DEBUG) console.log('Target nullified. was: '+target);
                target = null;
            }

            // generic outputs
            if (cmd.output) {
                cmd.output.forEach(function(message) {
                    if (message.text !== undefined) {
                        let type = (message.type !== undefined) ? message.type : 'message';
                        this(message.text, type);
                    }
                }, this.put);
            }

            // specific hint|canvas per argument
            // @FIXME : when man requested for an alias
            if (command === 'man' && argument && COMMANDS[argument] && COMMANDS[argument].aliasfor) {
                console.log('man requested for an alias '+argument);
                console.log(COMMANDS[argument]);
                argument = COMMANDS[argument].aliasfor;
            }

            // COMMANDS HANDLERS
            if (typeof commands[command] === "function") {
                var success = commands[command](this, argument, target);
                if (DEBUG) console.log('command:'+command+' argument:'+argument+' target:'+target+' RESULT:'+success);
                this.outputThreads();
            } else {
                // default outcome for commands without specifications
                this.set({
                    hint: (cmd.hint) ? cmd.hint : command,
                    scene: (cmd.canvas) ? cmd.canvas : this.has.scene
                });
            }

            return true;
        }

        this.put('unknown command');
        this.availableCommands();

        return false;
    }
}

export default Command;