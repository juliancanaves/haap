import React, { Component } from 'react';
import './Haap.css';
import './Terminal.css';
import './Profile.css';
import { DEFAULT_STATE } from './core/Consts';
import { syncStorage, getStorage } from './core/Storage';
import Canvas from './Canvas';
import Hint from './Hint';
import Profile from './Profile';
import Terminal from './Terminal';

class Haap extends Component {
    constructor(props) {
        super(props);
        var storedState = getStorage('haap');
        this.state = (storedState) ? storedState : syncStorage('haap', DEFAULT_STATE);
    }
    stateHandler = (object) => {
        this.setState(object);
        syncStorage('haap', this.state, object);
    }
    render() {
        return (
            <div className="wrapper">
                <div className="main">
                    <Canvas scene={this.state.scene} map={this.state.map} />
                    <Hint hint={this.state.hint} />
                    <Terminal stateHandler={this.stateHandler} haapState={this.state} />
                </div>
                <div className="side">
                    <Profile decker={this.state.decker} />
                </div>
            </div>
        );
    }
}

export default Haap;
