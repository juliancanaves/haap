import React from 'react';
import System from '../../src/core/System';
import {sampleSystemJson, sampleSystemMap} from '../../src/core/Fetcher.mock';

var systemProps = {
    fetcher: new Fetcher(),
    stateHandler: (object) => {
        console.log('stateHandler');
        console.log(object);
    }
};

describe('System', () => {
    
    it('loads system json', () => {

        fetch.mockResponse(JSON.stringify(sampleSystemJson));

        let systemTest = new System(systemProps);
        let result = systemTest.getSystem('sample');
        expect(result).toBe(true);
    });
    
    it('loads system map art', () => {

        fetch.mockResponse(sampleSystemMap);

        let systemTest = new System(systemProps);
        let result = systemTest.getMap('sample');
        expect(result).toBe(true);
    });

});
//# clear; npm test __tests__/core/System.test.js --watch