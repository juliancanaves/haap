import * as constant from '../../src/core/Consts';

describe('Consts', () => {
    it('exports OPPOSITE', () => {
        expect(typeof constant['OPPOSITE']).toEqual("object");
    });
    it('exports DIRECTIONS', () => {
        expect(typeof constant['DIRECTIONS']).toEqual("object");
    });
    it('exports SCRIPTS', () => {
        expect(typeof constant['SCRIPTS']).toEqual("object");
    });
    it('exports AVAILABLE', () => {
        expect(typeof constant['AVAILABLE']).toEqual("object");
    });
    it('exports SOFTWARE', () => {
        expect(typeof constant['SOFTWARE']).toEqual("object");
    });
    it('exports COMMANDS', () => {
        expect(typeof constant['COMMANDS']).toEqual("object");
    });
    it('exports NODE_TYPE', () => {
        expect(typeof constant['NODE_TYPE']).toEqual("object");
    });
    it('exports DECKERS', () => {
        expect(typeof constant['DECKERS']).toEqual("object");
    });
    it('exports TEXTS', () => {
        expect(typeof constant['TEXTS']).toEqual("object");
    });
});
// # clear; npm test __tests__/core/Consts.test.js --watch
// 13/01/2018 : pass