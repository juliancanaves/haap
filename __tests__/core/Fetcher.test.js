import React from 'react';
import {fetchMapServer} from '../../src/core/Fetcher';
import {mockedFetchMapServer, sampleSystemJson, sampleSystemMap} from '../../__mocks__/core/Fetcher.mock';

require('dotenv').config();

const mapServer = process.env.REACT_APP_MAP_SERVER;
const action = 'sample';
const fetchSpy = jest.spyOn(global, 'fetch');
const nock = require('nock');

describe('fetchMapServer', () => {
    it('calls map Server', async () => {
        var result = await fetchMapServer(action);
        expect(fetchSpy).toHaveBeenCalledWith(mapServer+'/'+action);
    });

    it('returns sample on exception', async () => {
        var result = await fetchMapServer(action);
        expect(result).toEqual(sampleSystemJson);
    });

    it('returns sample on map exception', async () => {
        var result = await fetchMapServer(action+'/map', true);
        expect(result).toEqual(sampleSystemMap);
    });
});

describe('mockedFetchMapServer', () => {
    it ('mockedFetchMapServer gives Json content', () => {
        var content = mockedFetchMapServer(action);
        expect(typeof content).toBe('object');
    });
    
    it ('mockedFetchMapServer gives  raw content as string', () => {
        var content = mockedFetchMapServer(action+'/map', true);
        expect(typeof content).toBe('string');
    });
});
//# clear; npm test __tests__/core/Fetcher.test.js --watch
// 13/01/2018 : pass