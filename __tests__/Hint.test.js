import React from 'react';
import ReactDOM from 'react-dom';
import Hint from './Hint';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Hint />, div);
});
