import React from 'react';
import ReactDOM from 'react-dom';
import Haap from '../src/Haap';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Haap />, div);
});
// # clear; npm test __tests__/Haap.test.js --watch