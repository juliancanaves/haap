import * as commands from '../../src/commands/hacks';

describe('Hack scripts index', () => {    
    it('exports all hacks', () => {
        expect(typeof commands['lock']).toEqual("function");
        expect(typeof commands['unlock']).toEqual("function");
        expect(typeof commands['go']).toEqual("function");
        expect(typeof commands['hack']).toEqual("function");
        expect(typeof commands['help']).toEqual("function");
        expect(typeof commands['info']).toEqual("function");
        expect(typeof commands['logout']).toEqual("function");
        expect(typeof commands['man']).toEqual("function");
        expect(typeof commands['software']).toEqual("function");
        expect(typeof commands['start']).toEqual("function");
        expect(typeof commands['unload']).toEqual("function");
        expect(typeof commands['unplug']).toEqual("function");
        expect(typeof commands['whoami']).toEqual("function");
    });
});
//# clear; npm test __tests__/commands/hacks/index.test.js --watch

