import * as commands from '../../src/commands';

describe('Commands index', () => {    
    it('exports all commands', () => {
        expect(typeof commands['access']).toEqual("function");
        expect(typeof commands['exits']).toEqual("function");
        expect(typeof commands['go']).toEqual("function");
        expect(typeof commands['hack']).toEqual("function");
        expect(typeof commands['help']).toEqual("function");
        expect(typeof commands['info']).toEqual("function");
        expect(typeof commands['logout']).toEqual("function");
        expect(typeof commands['man']).toEqual("function");
        expect(typeof commands['software']).toEqual("function");
        expect(typeof commands['start']).toEqual("function");
        expect(typeof commands['unload']).toEqual("function");
        expect(typeof commands['unplug']).toEqual("function");
        expect(typeof commands['whoami']).toEqual("function");
    });
});
//# clear; npm test __tests__/commands/index.test.js --watch
// 13/01/2018 : pass