// map server response for system json
export const sampleSystemJson = {
    "name": "Aztechnology",
    "level": 3,
    "message": "Welcome to Aztechnology @%USER_NAME%",
    "canvas": "corp/aztechnology",

    "nodes": [
        {
            "id": "san",
            "type": "san",
            "canvas": "node/access_01",
            "message" : "Pulsating light, lines of flowing information.",
            "hint": "Enter `info` to see available directions"
        },
        {
            "id": "spu1",
            "type": "spu",
            "canvas": "node/spu_01",
            "message" : "Looks like a corridor reflected by a mirror floor. Access control machine nearby.",
            "hint": "Enter `info control` to know about the access control"
        },
        {
            "id": "db1",
            "type": "db",
            "canvas": "node/db_01",
            "message" : "Blocks of info move in an out the big block.",
            "hint": "Enter `load decript` before `hack download` to get more money"
        },
        {
            "id": "iop1",
            "type": "iop",
            "canvas": "node/ioport_01",
            "message" : "A monitor of information to the outside. An entrance for entities to the inside.",
            "hint": "Nothing to do here YET"
        },
        {
            "id": "mod1",
            "type": "mod",
            "canvas": "node/module_01",
            "message" : "A sub-system that controls the elevators and escalators.",
            "hint": "Nothing to do here YET"
        },
        {
            "id": "cpu",
            "type": "cpu",
            "canvas": "node/cpu_01",
            "message" : "This is the mainframe of the system. Walls are full of beeping points of light.",
            "hint": "Here is where you enter `hack shutdown` to pass the tutorial"
        }
    ],
    "joins": [
        {
            "from": "san",
            "via": "N",
            "to": "spu1"
        },
        {
            "from": "spu1",
            "via": "E",
            "to": "db1"
        },
        {
            "from": "spu1",
            "via": "W",
            "to": "iop1"
        },
        {
            "from": "spu1",
            "via": "NE",
            "to": "cpu"
        },
        {
            "from": "spu1",
            "via": "NW",
            "to": "mod1"
        }
    ],
    "ice": [
        {
            "id": "control",
            "at": "spu1",
            "type": "barrier",
            "grade": "white",
            "level": 6,
            "keeps": "NE",
            "hint": "There is an access `control` machine on the way to `NE` Direction",
            "inspect": "Looks like an access control checker",
            "hint": "Maybe you can try that password program versus the ICE ",
            "canvas": "ice/access_control_01"
        },
        {
            "id": "serpent",
            "at": "db1",
            "type": "soldier",
            "grade": "grey",
            "level": 6,
            "keeps": "node",
            "hint": "A `serpent` guards the Database",
            "inspect": "Looks like a snake surrounding the big block",
            "canvas": "ice/interference_01",
            "hint": "Enter `run attack serpent` to overpass it"
        },
        {
            "id": "mike",
            "at": "iop1",
            "type": "soldier",
            "grade": "grey",
            "level": 6,
            "keeps": "node",
            "hint": "`Mike` is watching out that the Node does not get hacked",
            "inspect": "Looks like a policeman",
            "canvas": "ice/guard_01",
            "hint": "Enter `run attack mike` to overpass it"
        },
        {
            "id": "johnny",
            "at": "mod1",
            "type": "soldier",
            "grade": "grey",
            "level": 6,
            "keeps": "node",
            "hint": "`Johnny` is watching out that the Node does not get hacked",
            "inspect": "Looks like a policeman",
            "canvas": "ice/guard_01",
            "hint": "Enter `run attack johnny` to overpass it"
        },
        {
            "id": "rocco",
            "at": "cpu",
            "type": "soldier",
            "grade": "grey",
            "level": 6,
            "keeps": "node",
            "hint": "`Rocco` is watching out that the Node does not get hacked",
            "inspect": "Looks like a rough policeman",
            "canvas": "ice/guard_01",
            "hint": "Enter `run attack rocco` to overpass it"
        }
    ]
};

// map server response for system map art
export const sampleSystemMap = `<p>~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
~              N                                                                  ~
~          NW  |  NE                                                              ~
~            \ | /                                                                ~
~         W ---+--- E                                                             ~
~            / | \                                                                ~
~          SW  |  SE                                                              ~
~              S                                                                  ~
~                                                                                 ~
~                                                                                 ~
~                                                                                 ~
~                                                                                 ~
~                 |-------|            |-------|                                  ~
~                 |  mod1 |            |  cpu  |                                  ~
~                 |-------|            |-------|                                  ~
~                        \             /                                          ~
~                         \           /                                           ~
~         |-------|        \|-------|/         |-------|                          ~
~         | iop1  |---------| spu1  |----------|  db1  |                          ~
~         |-------|         |-------|          |-------|                          ~
~                               |                                                 ~
~                               |                                                 ~
~                           |-------|                                             ~
~                           |  san  |                                             ~
~                           |-------|                                             ~
~                                                                                 ~
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
</p>`;

export const mockedFetchMapServer = (action, raw = false) => {
    return (raw === true) ? sampleSystemMap : sampleSystemJson;
}